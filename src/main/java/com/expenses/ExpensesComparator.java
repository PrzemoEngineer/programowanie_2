package com.expenses;

import java.util.Comparator;

public class ExpensesComparator implements Comparator<Expense> {
    @Override
    public int compare(Expense o1, Expense o2) {
        return o1.getAmount().compareTo(o2.getAmount());
    }
}
