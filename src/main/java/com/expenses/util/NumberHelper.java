package com.expenses.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NumberHelper {
    public static boolean hasTwoOrLessDecimals(String number){
        String patternString = "[0-9]*(\\.[0-9]{0,2}0*)?";
        Pattern pattern = Pattern.compile(patternString);

        Matcher matcher = pattern.matcher(number);

        return  matcher.matches();
    }

    public static String numberToBinary(int number) {
        StringBuilder stringBuilder = new StringBuilder();

        if (number == 0) {
            return "0";
        } else {
            while (number > 0) {
                stringBuilder.append(number % 2);
                number = number / 2;
            }
            return stringBuilder.reverse().toString();
        }
    }
}
