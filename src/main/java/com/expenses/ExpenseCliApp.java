package com.expenses;

import com.expenses.io.ExpenseFileMapper;
import com.expenses.io.FileType;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.function.Consumer;

public class ExpenseCliApp {

    private final Scanner scanner;
    private final PrintStream printStream;
    private  final ExpenseService expenseService;

    public ExpenseCliApp(InputStream inputStream, PrintStream printStream, ExpenseService expenseService) {
        this.scanner = new Scanner(inputStream);
        this.printStream = printStream;
        this.expenseService = expenseService;
    }

    public void run(String dataFileName, FileType fileType) {
        loadData(dataFileName, fileType);
        run();
        saveData(dataFileName, fileType);
    }

    public void run() {
        printStream.println("Hello User!");

        boolean shutdownChosen = false;
        while (!shutdownChosen) {
            printStream.println("MAIN MENU");
            printStream.println("x - exit - application shutdown");
            printStream.println("a - add - add new expense");
            
            String option = scanner.nextLine();
            if (option.equals("x") || option.equals("exit")) {
                printStream.println("Shutting down");
                shutdownChosen = true;
            } else if (option.equals("a") || option.equals("add")) {
                tryToAddExpense();
            } else if (option.equals("l") || option.equals("list")) {
                listExpenses();
            }
        }

        printStream.println("Goodbye!");
    }

    public void tryToAddExpense(){
        printStream.println("Adding new Expense");

        System.out.println("Enter date in the dd-MM-yyyy format:");
        LocalDate date;
        String enteredDate = scanner.nextLine();
        try {
            date = LocalDate.parse(enteredDate, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        }catch (DateTimeParseException exception){
            printStream.println("Date + " + enteredDate + " is invalid.");
            return;
        }

        System.out.println("Enter amount:");
        BigDecimal amount;
        String enteredAmount = scanner.nextLine();
        try {
            amount = new BigDecimal(enteredAmount);
        }catch (NumberFormatException exception){
            printStream.println("Entered amount " + enteredAmount + " is invalid.");
            return;
        }

        System.out.println("Enter non-empty location:");
        String location = scanner.nextLine();
        if (StringUtils.isBlank(location)){
            System.out.println("Entered location is blank.");
            return;
        }

        printStream.println("Enter category (can be empty):");
        String category = scanner.nextLine();

        Expense expense = null;
        try {
            expense = Expense.from(amount, date, location, category);
        }catch (InvalidExpenseException exception){
            String message = "Could not create expense: "
                    + (StringUtils.isBlank(exception.getMessage())
                    ? "Unknown error."
                    : exception.getMessage());
            printStream.println(message);
        }

        printStream.println("Adding expense: " + expense.toString());
        expenseService.addExpense(expense);
    }

    void listExpenses() {
        Consumer<Expense> expensePrinterAnonClassObj = new Consumer<Expense>() {
            @Override
            public void accept(Expense expense) {
                printStream.println(expense);
            }
        };

        Consumer<Expense> expensePrinterLambda = (expense) -> printStream.println(expense);
        Consumer<Expense> expensePrinterMethodRef = printStream::println;

        Set<Expense> expenses = expenseService.getExpenses();
        printStream.println("Listing expenses");
        expenses.forEach(printStream::println);
    }

    private void loadData(String filename, FileType fileType) {
        printStream.println("Loading data from " + filename);

        Set<Expense> expenses = new HashSet<>();
        try {
            ExpenseFileMapper expenseFileMapper = new ExpenseFileMapper();
            expenses = expenseFileMapper.readFromFile(filename);
        }catch (IOException exception){
            printStream.println("Could not load data form " + filename + ": " + exception.getLocalizedMessage());
        }

        expenses.forEach(expenseService::addExpense);
        printStream.println("Successfully loaded " + expenses.size() + " expenses.");
    }

    private void saveData(String fileName, FileType fileType) {
        printStream.println("Saving data to " + fileName);

        try {
            ExpenseFileMapper expenseFileMapper = new ExpenseFileMapper();
            expenseFileMapper.writeToFile(fileName, fileType,expenseService.getExpenses());
        } catch (IOException exception) {

        }
    }
}
