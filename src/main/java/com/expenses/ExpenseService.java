package com.expenses;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class ExpenseService {
    private Set<Expense> expenses = new HashSet<>();

    public void addExpense(Expense expense) {
        expenses.add(expense);
    }

    public Set<Expense> getExpenses(){
        return new HashSet<>(expenses);
    }

    public String toString(){
        StringBuilder message = new StringBuilder("Expenses:\n");

        for (Expense expense : expenses){
            message.append(expense).append("\n");
        }
//        expenses.forEach(new Consumer<Expense>() {
//            @Override
//            public void accept(Expense expense) {
//                message.append(expense).append("\n");
//            }
//        });
//
//        expenses.forEach(expense -> message.append(expenses).append("\n"));

        return message.toString();
    }

    public Set<Expense> findExpensesByDate(LocalDate date){
        Predicate<Expense> expensesFromGivenDatePredicate = expense -> expense.getDate().equals(date);

        return expenses.stream()
                .filter(expensesFromGivenDatePredicate)
                .collect(Collectors.toSet());
    }

    public Set<Expense> findExpensesInRange(LocalDate from, LocalDate to){
        Predicate<Expense> expensesFromGivenRangePredicate = expense -> expense.getDate().isAfter(from) && expense.getDate().isBefore(to);
        return expenses.stream()
                .filter(expensesFromGivenRangePredicate)
                .collect(Collectors.toSet());
    }

    public Set<Expense> findExpensesByPlace(String place){
        Predicate<Expense> findExpensesByPlacePredicate = expense -> expense.getPlace().equals(place);

        return expenses.stream()
                .filter(findExpensesByPlacePredicate)
                .collect(Collectors.toSet());
    }

    public Set<Expense> findExpensesByCategory(String category){
        Predicate<Expense> findExpensesByCategoryPredicate = expense -> expense.getCategory().equals(category);

        return expenses.stream()
                .filter(findExpensesByCategoryPredicate)
                .collect(Collectors.toSet());
    }

    public BigDecimal countAverageInGivenDateRange(LocalDate from, LocalDate to){
        Set<Expense> expensesFromGivenRange = this.findExpensesInRange(from,to);

        BigDecimal average;
        BigDecimal amountSum = BigDecimal.ZERO;

        for (Expense expense : expensesFromGivenRange) {
            amountSum = amountSum.add(expense.getAmount());
        }

        average = amountSum.divide(BigDecimal.valueOf(expensesFromGivenRange.size()),2, RoundingMode.HALF_UP);

        return average;
    }

    public BigDecimal findHighestExpenseInCategory(String category){
        Set<Expense> expensesFromGivenCategory = findExpensesByCategory(category);
        Comparator<Expense> highestExpenseInCategoryComparator = Comparator.comparing(Expense::getAmount);

        return expensesFromGivenCategory.stream()
                .max(highestExpenseInCategoryComparator)
                .get()
                .getAmount();
    }

    public Set<String> makeListOfAllCategories(){
        Set<String> categoriesList = new HashSet<>();

        for (Expense expense : expenses) {
                categoriesList.add(expense.getCategory());
        }

        return categoriesList;
    }

    public Map<String, BigDecimal> findHighestExpenseInEachCategory(){
        Map<String, BigDecimal> highestExpenseInEachCategory = new HashMap();
        Set<String> categoriesList = makeListOfAllCategories();

        for (String category : categoriesList) {
            BigDecimal highestAmount = findHighestExpenseInCategory(category);
            highestExpenseInEachCategory.put(category, highestAmount);
        }

        return  highestExpenseInEachCategory;
    }

    public BigDecimal countAverageExpenseInCategory(String category){
        BigDecimal average;
        BigDecimal amountSum = BigDecimal.ZERO;
        Set<Expense> expensesFromGivenCategory = findExpensesByCategory(category);

        for (Expense expense : expensesFromGivenCategory) {
            amountSum = amountSum.add(expense.getAmount());
        }

        if (expensesFromGivenCategory.size() == 0){
            average = BigDecimal.ZERO;
        }else {
            average = amountSum .divide(BigDecimal.valueOf(expensesFromGivenCategory.size()), 2, RoundingMode.HALF_UP);
        }

        return average;
    }

    public Map<String, BigDecimal> findAverageExpenseInEachCategory(){
        Map<String, BigDecimal> averageExpenseInEachCategory = new HashMap();
        Set<String> categoriesList = makeListOfAllCategories();

        for (String category : categoriesList) {
            BigDecimal averageAmount = countAverageExpenseInCategory(category);

            averageExpenseInEachCategory.put(category, averageAmount);
        }

        return averageExpenseInEachCategory;
    }

    public Set<Expense> findExpensesBefore(LocalDate date){
        Set<Expense> expensesBeforeDate = new HashSet<>();

        ExpenseIsBeforeDatePredicate beforeDatePredicate = new ExpenseIsBeforeDatePredicate(date);
        for (Expense expense : expenses){
            if (beforeDatePredicate.test(expense)){
                expensesBeforeDate.add(expense);
            }

            return expensesBeforeDate;
        }

        return expensesBeforeDate;
    }

    public Set<Expense> findExpensesBeforeStream(LocalDate date){
        ExpenseIsBeforeDatePredicate beforeDatePredicate = new ExpenseIsBeforeDatePredicate(date);

        Set<Expense> expensesBeforeDate = expenses.stream()
                .filter(beforeDatePredicate)
                .collect(Collectors.toSet());

        return expensesBeforeDate;
    }

    public Set<Expense> findExpensesBeforeStreamAnonymousClass(LocalDate date){
        Predicate<Expense> beforeDatePredicate = new Predicate<Expense>() {
            @Override
            public boolean test(Expense expense) {
                return expense.getDate().isBefore(date);
            }
        };

        return expenses.stream()
                .filter(beforeDatePredicate)
                .collect(Collectors.toSet());
    }

    public Set<Expense> findExpensesBeforeStreamLambda(LocalDate date){
        Predicate<Expense> beforeDatePredicate = expense -> expense.getDate().isBefore(date);

        return  expenses.stream()
                .filter(beforeDatePredicate)
                .collect(Collectors.toSet());
    }

    public Set<Expense> findNLargestExpensesExternalClass (int n){
        ExpensesComparator expensesComparator = new ExpensesComparator();

        Set<Expense> nLargestExpenses = expenses.stream()
                .sorted(expensesComparator)
                .limit(n)
                .collect(Collectors.toSet());

        return nLargestExpenses;
    }

    public Set<Expense> findNLargestExpensesAnonymousClass (int n) {
        Comparator<Expense> expenseComparator = new Comparator<Expense>() {
            @Override
            public int compare(Expense o1, Expense o2) {
                return o1.getAmount().compareTo(o2.getAmount());
            }
        };

        return expenses.stream()
                .sorted(expenseComparator)
                .limit(n)
                .collect(Collectors.toSet());
    }

    public Set<Expense> findNLargestExpensesLambda (int n) {
        Comparator<Expense> expenseComparator = (o1, o2) -> o1.getAmount().compareTo(o2.getAmount());

        return expenses.stream()
                .sorted(expenseComparator)
                .limit(n)
                .collect(Collectors.toSet());
    }

    public Set<Expense> findNLargestExpenses (int n) {//Method reference
        Comparator<Expense> expenseComparator = Comparator.comparing(Expense::getAmount);

        return expenses.stream()
                .sorted(expenseComparator)
                .limit(n)
                .collect(Collectors.toSet());
    }
}
