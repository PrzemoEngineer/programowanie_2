package com.expenses.io;

import com.expenses.Expense;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

public class ExpenseFileMapper {
    public void writeToFile(String fileName,FileType fileType,  Set<Expense> expenses) throws IOException {
        // todo try to guess file type from filename instead of getting it as an argument
        ExpenseMapper expenseCsvMapper = fileType.getMapper();

        try (FileWriter fileWriter = new FileWriter(fileName)) {
            expenseCsvMapper.write(expenses, fileWriter); // try with resources
        }
    }

    public Set<Expense> readFromFile(String fileName) throws IOException {
        ExpenseCsvMapper expenseCsvMapper = new ExpenseCsvMapper();

        try (FileReader fileReader = new FileReader(fileName)) {
            return expenseCsvMapper.read(fileReader);
        }
    }
}
