package com.expenses.io;

import com.expenses.Expense;
import com.expenses.InvalidExpenseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashSet;
import java.util.Set;


public class ExpenseCsvMapper implements ExpenseMapper{
    private static final String DELIMITER = ",";

    private static final DateTimeFormatter DATE_FORMATTER =
            DateTimeFormatter.ofPattern("dd-MM-yyyy");

    @Override
    public Set<Expense> read(Reader reader) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(reader);
        Set<Expense> expenses = new HashSet<>();
        //Skip header
        bufferedReader.readLine();

        String line = bufferedReader.readLine();
        while (line != null) {
            expenses.add(mapFromCsvRow(line));
            line = bufferedReader.readLine();
        }
        return expenses;
    }

    @Override
    public void write(Set<Expense> expenses, Writer writer) throws IOException {
        writer.write("amount,date,location,category\n");

        for (Expense expense : expenses) {
            writer.write(mapToCsvRow(expense));
        }
    }

    private static String mapToCsvRow(Expense expense) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(expense.getAmount()).append(",");
        stringBuilder.append(DATE_FORMATTER.format(expense.getDate())).append(",");
        stringBuilder.append(expense.getPlace()).append(",");
        stringBuilder.append(expense.getCategory() == null ? "" : expense.getCategory());
        stringBuilder.append("\n");

        return stringBuilder.toString();
    }

    private static Expense mapFromCsvRow(String row) throws IOException {
        String[] columns = row.split(DELIMITER);

        if (columns.length < 3){
            throw new IOException("Row has too few columns: " + row + "]");
        }

        String amountText = columns[0];
        BigDecimal amount;
        try {
            amount = new BigDecimal(amountText);
        } catch (NumberFormatException exception) {
            throw new IOException("Cannot parse number in row: [" + amountText + "]", exception);
        }

        String dateText = columns[1];
        LocalDate date;
        try {
            date = LocalDate.parse(dateText, DATE_FORMATTER);
        } catch (DateTimeParseException exception) {
            throw  new IOException("Cannot parse date in row: [" + dateText + "]", exception);
        }

        String location = columns[2];

        String category = null;

        if (columns.length > 3) {
            category = columns[3];
        }

        Expense expense;
        try {
            expense = Expense.from(amount, date, location, category);
        } catch (InvalidExpenseException exception) {
            throw new IOException("Cannot create expense", exception);
        }

        return expense;
    }
}
