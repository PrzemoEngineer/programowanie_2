package com.expenses;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class Expense {
    private BigDecimal amount;
    private LocalDate date;
    private String place;
    private String category;

    private Expense(BigDecimal amount, LocalDate date, String place, String category) {
        this.amount = amount;
        this.date = date;
        this.place = place;
        this.category = category;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getPlace() {
        return place;
    }

    public String getCategory() {
        return category;
    }

    @Override
    public String toString(){
        DateTimeFormatter dateFormat = DateTimeFormatter
                .ofPattern("dd-MM-yyyy G");
        String message = "{%s, %s, %s, %s}";

        return String.format(message, date.format(dateFormat), amount, place, category);
    }

    public static Expense from(BigDecimal amount, LocalDate date, String place, String category) throws InvalidExpenseException {
        if (amount.compareTo(BigDecimal.ZERO) <= 0){
            throw new InvalidExpenseException("Expense amount should not be negative.", amount);
        }
        if (date == null || date.isAfter(LocalDate.now())){
            throw new InvalidExpenseException("Date should not be in future.", amount);
        }
        if (place == null || place.isBlank()){
            throw new InvalidExpenseException("Place should not be blank.", amount);
        }
        if (!twoOrLessDecimals(amount.toPlainString())){
            throw new InvalidExpenseException("Amount should have only two decimals.", amount);
        }

        return new Expense(amount, date, place, category);
    }

    public static boolean twoOrLessDecimals(String number){
        String[] numberTable = number.split("[.,]");
        
        if (numberTable.length < 2) {
            return  true;
        }

        String toAnalyse = numberTable[1];
        for (int i = toAnalyse.length() - 1; i >= 0 ; --i) {
              char digit = toAnalyse.charAt(i);
              if (digit != '0') {
                  return i < 2;
              }
        }

        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Expense expense = (Expense) o;
        return amount.equals(expense.amount) &&
                date.equals(expense.date) &&
                place.equals(expense.place) &&
                category.equals(expense.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount, date, place, category);
    }
}
