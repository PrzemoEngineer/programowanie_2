package com.expenses;

import java.math.BigDecimal;

public class InvalidExpenseException extends Exception{

    private double amount;

    public InvalidExpenseException(String message, BigDecimal amount){
        super(message + " Given amount (" + amount + ") is invalid. ");


    }
}
