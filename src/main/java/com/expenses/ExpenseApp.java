package com.expenses;

import java.math.BigDecimal;
import java.time.LocalDate;

public class ExpenseApp {
    public static void main(String[] args) throws InvalidExpenseException {
//        Expense expense = new Expense(100, LocalDate.now(), "restaurant", "food");
//        Expense expense2 = new Expense(101, LocalDate.now(), "restaurant", "food");
//        Expense expense3 = new Expense(102, LocalDate.now(), "restaurant", "food");

        ExpenseService expenseService = new ExpenseService();
//        expenseService.addExpense(expense);
//        expenseService.addExpense(expense2);
//        expenseService.addExpense(expense3);

        Expense expense1 = Expense.from(BigDecimal.valueOf(100.1201), LocalDate.now().minusDays(1), "loc", "Cat");

        expenseService.addExpense(expense1);
        System.out.println(expenseService);
    }
}
