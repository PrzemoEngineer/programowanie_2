package com.expenses;

import java.time.LocalDate;
import java.util.function.Predicate;

public class ExpenseIsBeforeDatePredicate implements Predicate<Expense> {

    private final LocalDate compareDate;

    public ExpenseIsBeforeDatePredicate(LocalDate compareDate) {
        this.compareDate = compareDate;
    }

    @Override
    public boolean test(Expense expense) {
        return expense.getDate().isBefore(compareDate);
    }
}
