package com.expenses;

import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Map;
import java.util.Set;


import static org.junit.jupiter.api.Assertions.*;

class ExpenseTest {

    @ParameterizedTest
    @ValueSource(doubles = {3.001, 3.3001, 3.010000009, 3.0201})
    void shouldNotAllowAmountsWithMoreThan2DecimalPlaces(double amount){
        //When
        Executable createExpenseWithInvalidAmount = () -> Expense.from(
                BigDecimal.valueOf(amount),
                LocalDate.now(),
                "loc",
                "Cat"
        );

        //Then
        assertThrows(InvalidExpenseException.class, createExpenseWithInvalidAmount);
    }

    @ParameterizedTest
    @CsvSource({"3.12", "3.01", "3.30", "3.2", "3"})
    void shouldAllowAmountsWithTwoOrLessDecimalPlaces(BigDecimal amount){
        //When
        Executable createExpenseWithValidAmount = () -> Expense.from(
                amount,
                LocalDate.now(),
                "loc",
                "Cat"
        );

        //Then
        assertDoesNotThrow(createExpenseWithValidAmount);
    }

    @Test
    void shouldNotAllowExpensesInFuture(){
        //Given
        LocalDate dateInFuture = LocalDate.now().plusYears(1);

        //When
        Executable createExpenseInFuture = () -> Expense.from(BigDecimal.valueOf(100),
                dateInFuture,
                "Location",
                "Category");

        //Then
        assertThrows(InvalidExpenseException.class, createExpenseInFuture);
    }

    @Test
    void shouldNotAllowedNullDate(){
        //When
        Executable createNullDateExpense = () -> Expense.from(BigDecimal.valueOf(100),
                null,
                "Location",
                "Category");

        //Then
        assertThrows(InvalidExpenseException.class, createNullDateExpense);
    }

    @Test
    void shouldReturnExpensesWithRequestedDate() throws InvalidExpenseException {
        //Given
        LocalDate requestedDate = LocalDate.now().minusDays(5);

        Expense expense1 = Expense.from(BigDecimal.valueOf(200), requestedDate, "loc", "Cat");
        Expense expense2 = Expense.from(BigDecimal.valueOf(100), requestedDate.minusDays(10), "loc", "Cat");
        Expense expense3 = Expense.from(BigDecimal.valueOf(50), requestedDate, "loc", "Cat");

        ExpenseService expenseService = new ExpenseService();
        expenseService.addExpense(expense1);
        expenseService.addExpense(expense2);
        expenseService.addExpense(expense3);


        //When
        Set<Expense> foundByDate = expenseService.findExpensesByDate(requestedDate);

        //Then
        assertEquals(2, foundByDate.size());
        assertTrue(foundByDate.contains(expense1));
        assertTrue(foundByDate.contains(expense3));
    }

    @Test
    void shouldReturnExpensesWithRequestedDateRange() throws InvalidExpenseException {
        //Given
        LocalDate rangeStartDate = LocalDate.now().minusDays(5);
        LocalDate rangeFinalDate = LocalDate.now();

        Expense expense1 = Expense.from(BigDecimal.valueOf(200), rangeStartDate.plusDays(1), "loc", "Cat");
        Expense expense2 = Expense.from(BigDecimal.valueOf(100), rangeFinalDate.minusDays(1), "loc", "Cat");
        Expense expense3 = Expense.from(BigDecimal.valueOf(150), LocalDate.now(), "loc", "Cat");
        Expense expense4 = Expense.from(BigDecimal.valueOf(250), LocalDate.now(), "loc", "Cat");

        ExpenseService expenseService = new ExpenseService();
        expenseService.addExpense(expense1);
        expenseService.addExpense(expense2);
        expenseService.addExpense(expense3);
        expenseService.addExpense(expense4);

        //When
        Set<Expense> foundInDateRange = expenseService.findExpensesInRange(rangeStartDate, rangeFinalDate);

        //Then
        assertEquals(2, foundInDateRange.size());
        assertTrue(foundInDateRange.contains(expense1));
        assertTrue(foundInDateRange.contains(expense2));
    }

    @ParameterizedTest
    @ValueSource(strings = {"Gas Station", "Grocery"})
    void shouldReturnExpensesWithRequestedPlace(String place) throws InvalidExpenseException {
        //Given
        Expense expense1 = Expense.from(BigDecimal.valueOf(100), LocalDate.now(), "loc", "Cat");
        Expense expense2 = Expense.from(BigDecimal.valueOf(200), LocalDate.now(), place, "Cat");
        Expense expense3 = Expense.from(BigDecimal.valueOf(300), LocalDate.now(), "loc", "Cat");
        Expense expense4 = Expense.from(BigDecimal.valueOf(400), LocalDate.now(), place, "Cat");

        ExpenseService expenseService = new ExpenseService();
        expenseService.addExpense(expense1);
        expenseService.addExpense(expense2);
        expenseService.addExpense(expense3);
        expenseService.addExpense(expense4);

        //When
        Set<Expense> foundByPlace = expenseService.findExpensesByPlace(place);

        //Then
        assertEquals(2, foundByPlace.size());
        assertTrue(foundByPlace.contains(expense2));
        assertTrue(foundByPlace.contains(expense4));
    }

    @ParameterizedTest
    @ValueSource(strings = {"Car expenses", "Groceries"})
    void shouldReturnExpensesWithRequestedCategory(String category) throws InvalidExpenseException {
        //Given
        Expense expense1 = Expense.from(BigDecimal.valueOf(200), LocalDate.now(), "loc", category);
        Expense expense2 = Expense.from(BigDecimal.valueOf(100), LocalDate.now(), "loc", "Cat");
        Expense expense3 = Expense.from(BigDecimal.valueOf(300), LocalDate.now(), "loc", category);
        Expense expense4 = Expense.from(BigDecimal.valueOf(400), LocalDate.now(), "loc", "Cat");

        ExpenseService expenseService = new ExpenseService();
        expenseService.addExpense(expense1);
        expenseService.addExpense(expense2);
        expenseService.addExpense(expense3);
        expenseService.addExpense(expense4);

        //When
        Set<Expense> foundByCategory = expenseService.findExpensesByCategory(category);

        //Then
        assertEquals(2, foundByCategory.size());
        assertTrue(foundByCategory.contains(expense1));
        assertTrue(foundByCategory.contains(expense3));
    }

    @ParameterizedTest
    @CsvSource({"1.5, 5, 3.5, 3.33", "4, 8, 2.6, 4.87"})
    void shouldAverageExpensesAmountWithGivenDateRange(BigDecimal amount1, BigDecimal amount2, BigDecimal amount3, BigDecimal average) throws InvalidExpenseException {
        //Given
        Expense expense1 = Expense.from(amount1, LocalDate.now().minusDays(3), "loc", "Cat");
        Expense expense2 = Expense.from(amount2, LocalDate.now().minusDays(5), "loc", "Cat");
        Expense expense3 = Expense.from(amount3, LocalDate.now().minusDays(1), "loc", "Cat");

        ExpenseService expenseService = new ExpenseService();

        expenseService.addExpense(expense1);
        expenseService.addExpense(expense2);
        expenseService.addExpense(expense3);

        //When
        BigDecimal resultAverage = expenseService.countAverageInGivenDateRange(LocalDate.now().minusDays(10), LocalDate.now());

        //Then
        assertEquals(average, resultAverage);
    }

    @ParameterizedTest
    @CsvSource({"category1, 10000", "category2, 30.1", "category3, 50.05"})
    void shouldReturnHighestExpenseAmountInGivenCategory(String category, BigDecimal highestAmount) throws InvalidExpenseException{
        //Given
        Expense expense1 = Expense.from(highestAmount.subtract(BigDecimal.valueOf(0.01)), LocalDate.now(), "loc", category);
        Expense expense2 = Expense.from(highestAmount.subtract(BigDecimal.valueOf(1)), LocalDate.now(), "loc", category);
        Expense expense3 = Expense.from(highestAmount.subtract(BigDecimal.valueOf(10)), LocalDate.now(), "loc", "some other category");
        Expense expense4 = Expense.from(highestAmount, LocalDate.now(), "loc", category);

        ExpenseService expenseService = new ExpenseService();
        expenseService.addExpense(expense1);
        expenseService.addExpense(expense2);
        expenseService.addExpense(expense3);
        expenseService.addExpense(expense4);

        //When
        BigDecimal result = expenseService.findHighestExpenseInCategory(category);

        //Then
        assertEquals(highestAmount, result);
    }

    @ParameterizedTest
    @CsvSource({"cat1, cat2, cat3, 5, 8, 10", "shop, car, house, 10.50, 15.20, 20.30", "food, water, other, 20.01, 5.05, 10.05"})
    void shouldReturnMapOfHighestAmountInEachCategory(String category1, String category2, String category3,
                                                      BigDecimal highestAmount1, BigDecimal highestAmount2, BigDecimal highestAmount3) throws InvalidExpenseException{
        //Given
        Expense expense1 = Expense.from(highestAmount1, LocalDate.now(), "loc", category1);
        Expense expense2 = Expense.from(highestAmount1.subtract(BigDecimal.valueOf(1)), LocalDate.now(), "loc", category1);
        Expense expense3 = Expense.from(highestAmount2, LocalDate.now(), "loc", category2);
        Expense expense4 = Expense.from(highestAmount2.subtract(BigDecimal.valueOf(3)), LocalDate.now(), "loc", category2);
        Expense expense5 = Expense.from(highestAmount3, LocalDate.now(), "loc", category3);
        Expense expense6 = Expense.from(highestAmount3.subtract(BigDecimal.valueOf(1)), LocalDate.now(), "loc", category3);

        ExpenseService expenseService = new ExpenseService();
        expenseService.addExpense(expense1);
        expenseService.addExpense(expense2);
        expenseService.addExpense(expense3);
        expenseService.addExpense(expense4);
        expenseService.addExpense(expense5);
        expenseService.addExpense(expense6);

        //When
        Map<String, BigDecimal> resultMap = expenseService.findHighestExpenseInEachCategory();

        //Then
        assertTrue(resultMap.containsKey(category1));
        assertTrue(resultMap.containsKey(category2));
        assertTrue(resultMap.containsKey(category3));

        assertTrue(resultMap.containsValue(highestAmount1));
        assertTrue(resultMap.containsValue(highestAmount2));
        assertTrue(resultMap.containsValue(highestAmount3));

        assertEquals(highestAmount1, resultMap.get(category1));
        assertEquals(highestAmount2, resultMap.get(category2));
        assertEquals(highestAmount3, resultMap.get(category3));

        assertTrue(resultMap.size() == 3);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/testData.csv", numLinesToSkip = 1)
    void shouldReturnMapOfAverageAmountInEachCategory(String category1, String category2, BigDecimal amount1, BigDecimal amount2, BigDecimal amount3 , BigDecimal average) throws InvalidExpenseException {
        //Given
        Expense expense1 = Expense.from(amount1, LocalDate.now(), "loc", category1);
        Expense expense2 = Expense.from(amount2, LocalDate.now(), "loc", category1);
        Expense expense3 = Expense.from(amount3, LocalDate.now(), "loc", category1);
        Expense expense4 = Expense.from(amount3, LocalDate.now(), "loc", category2);
        Expense expense5 = Expense.from(amount2, LocalDate.now(), "loc", category2);
        Expense expense6 = Expense.from(amount1, LocalDate.now(), "loc", category2);

        ExpenseService expenseService = new ExpenseService();

        expenseService.addExpense(expense1);
        expenseService.addExpense(expense2);
        expenseService.addExpense(expense3);
        expenseService.addExpense(expense4);
        expenseService.addExpense(expense5);
        expenseService.addExpense(expense6);

        //When
        Map resultMap = expenseService.findAverageExpenseInEachCategory();


        //Then
        assertTrue(resultMap.containsKey(category1));
        assertTrue(resultMap.containsKey(category2));


        assertTrue(resultMap.containsValue(average));


        assertEquals(resultMap.get(category1), average);
        assertEquals(resultMap.get(category2), average);


        assertTrue(resultMap.size() == 2);
    }

    @ParameterizedTest
    @CsvSource({"100, 1", "200, 2", "300, 3"})
    void shouldReturnNLargestExpenses(BigDecimal amount, int n) throws InvalidExpenseException {
        //Given
        Expense expense1 = Expense.from(amount.subtract(BigDecimal.valueOf(1)),LocalDate.now(), "shop", "groceries");
        Expense expense2 = Expense.from(amount.subtract(BigDecimal.valueOf(2)),LocalDate.now(), "shop", "groceries");
        Expense expense3 = Expense.from(amount.subtract(BigDecimal.valueOf(5)),LocalDate.now(), "shop", "groceries");
        Expense expense4 = Expense.from(amount,LocalDate.now(), "shop", "groceries");
        Expense expense5 = Expense.from(amount.subtract(BigDecimal.valueOf(10)),LocalDate.now(), "shop", "groceries");

        ExpenseService expenseService = new ExpenseService();
        expenseService.addExpense(expense1);
        expenseService.addExpense(expense2);
        expenseService.addExpense(expense3);
        expenseService.addExpense(expense4);
        expenseService.addExpense(expense5);

        //When
        Set<Expense> nLargestExpenses = expenseService.findNLargestExpenses(n);
        int result = n;

        //Then
        assertEquals(result,nLargestExpenses.size());
    }
}