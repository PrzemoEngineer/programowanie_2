package com.expenses.io;

import com.expenses.Expense;
import org.junit.jupiter.api.Test;

import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class ExpenseCsvMapperTest {

    @Test
    void shouldWriteExpenses() throws Exception {
        //Given
        StringWriter stringWriter = new StringWriter();
        ExpenseCsvMapper expenseCsvMapper = new ExpenseCsvMapper();

        Set<Expense> expenses = Set.of(
                Expense.from(
                        BigDecimal.valueOf(100),
                        LocalDate.of(2000, Month.APRIL, 3),
                        "location1",
                        "category1"),
                Expense.from(
                        BigDecimal.valueOf(200),
                        LocalDate.of(2010, Month.MAY, 20),
                        "location2",
                        "category2")
        );
        String expectedHeader = "amount,date,location,category";
        String expectedLine1 = "100,03-04-2000,location1,category1";
        String expectedLine2 = "200,20-05-2010,location2,category2";

        //When
        expenseCsvMapper.write(expenses, stringWriter);

        //Then
        List<String> csvLines = Arrays.asList(stringWriter.toString().split("\n"));

        assertEquals(3, csvLines.size());
        assertEquals(expectedHeader, csvLines.get(0));
        assertTrue(csvLines.contains(expectedLine1),
                csvLines + "should contain: " + expectedLine1);
        assertTrue(csvLines.contains(expectedLine2),
                csvLines + "should contain: " + expectedLine2);
    }

    @Test
    void shouldReadExpenses() throws Exception {
        //Given
        ExpenseCsvMapper expenseCsvMapper = new ExpenseCsvMapper();
        String expensesCsv = "amount,date,location,category\n" +
            "100,03-04-2000,location1,category1\n" +
            "200,20-05-2010,location2,category2\n";

        Expense expense1 = Expense.from(
                BigDecimal.valueOf(100),
                LocalDate.of(2000, Month.APRIL, 3),
                "location1",
                "category1");
        Expense expense2 = Expense.from(
                BigDecimal.valueOf(200),
                LocalDate.of(2010, Month.MAY, 20),
                "location2",
                "category2");


        //When
        Set<Expense> actualExpenses = expenseCsvMapper.read(new StringReader(expensesCsv));

        //Then
        assertEquals(2, actualExpenses.size());
        assertTrue(actualExpenses.contains(expense1), "\nSet: " + actualExpenses + "\nshould contain:\n" + expense1);
        assertTrue(actualExpenses.contains(expense2), "\nSet: " + actualExpenses + "\nshould contain:\n" + expense2);
    }

}