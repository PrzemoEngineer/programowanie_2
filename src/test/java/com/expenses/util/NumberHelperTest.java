package com.expenses.util;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class NumberHelperTest {

    static Stream<Arguments> getNumbers(){
        return Stream.of(
                Arguments.of("12", true),
                Arguments.of("12.", true),
                Arguments.of("12.1", true),
                Arguments.of("12.12", true),
                Arguments.of("12.1221", false),
                Arguments.of("12.123", false),
                Arguments.of("12.1201", false)
        );
    }

    @ParameterizedTest(name = "Number {0} has two or less decimal places: {1}")
    @MethodSource("getNumbers")
    void shouldDetectValidNumberOfDecimalPlaces(String number, boolean expectedResult){
        //When
        boolean actualResult = NumberHelper.hasTwoOrLessDecimals(number);

        //Then
        assertEquals(expectedResult, actualResult);
    }

    @Disabled
    @Test
    void doublePrecisionCheck(){
        //Given
        double a = 0.1;
        double b = 0.2;
        double expectedResult = 0.3;

        //When
        double actualResult = a + b;

        //Then
        assertEquals(expectedResult,actualResult);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3, 5, 21, 128, 513})
    void shouldReturnNumberInBinary(int number) {
        //Given
        String actualString;
        String expectedString;

        //When
        actualString = NumberHelper.numberToBinary(number);
        expectedString = Integer.toBinaryString(number);

        //Then
        assertEquals(expectedString, actualString);
    }

}