package com.expenses.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;

public class BinaryOperationsTest {
    public static  boolean isNotBlank(String text){
        // short circuiting - podwójne && sprawdza najpierw lewo a potem prawo
        return text != null && !text.isBlank();
    }

    public static boolean isNotBlankBinary(String text){
        // binarny operator
        return text != null & !text.isBlank(); // - zrobi jedną funkcję potem drugą i je sprawdzi binarnie - inaczej niż w przypadku && - WAŻNE!!!!
    }

    //             110
    //             100
    // 110 & 100 = 100 - sprawdza obie pozyzycje i jak jest w obu 1 a jak nie to 0
    // 0 & 0

    @Test
    void checkIsNotBlank() {
        String nullText = null;

        assertFalse(isNotBlank(nullText));
        //assertFalse(isNotBlankBinary(nullText)); // - NullPointerException
    }

    @Test
    void checkBinaryAnd() {
        int four = 4;
        int six = 6;

        System.out.println(Integer.toBinaryString(four));
        System.out.println(Integer.toBinaryString(six));
        System.out.println("-------------------");
        System.out.println(Integer.toBinaryString(four | six));
    }
}
